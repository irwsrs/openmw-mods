# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import shutil
from pybuild import Pybuild1, version_gt, use_reduce


class System(Pybuild1):
    """
    Pybuild Class for system-installed executables
    """

    RESTRICT = "fetch"
    RELEASE_PAGE: str

    def mod_pretend(self):
        super().mod_pretend()
        exec_path = shutil.which(self.MN)

        if not exec_path:
            string = (
                f"Unable to find executable {self.MN}\n"
                "Please download and install it using your package manager\n"
                "or from the following location(s):\n"
            )
            for source in use_reduce(self.RELEASE_PAGE, self.USE):
                string += f"    {source}\n"

            self.warn(string)

    def get_version(self) -> str:
        """Returns the version string for the software"""
        return self.MV

    def src_prepare(self):
        super().src_prepare()
        exec_path = shutil.which(self.MN)

        if not exec_path:
            raise Exception(f"Could not find executable {self.MN}")

        version = self.get_version()
        if version_gt(self.MV, version):
            raise Exception(
                f"{self.MN} has version {version}, but we require >= {self.MV}"
            )
